from pymongo import MongoClient
import json

username = "root"  # 替换为你的MongoDB用户名
password = "root"  # 替换为你的MongoDB密码
host = "192.168.214.1"
port = 27018
authSource  = "admin"  # 默认的身份验证数据库是admin，替换为你的身份验证数据库

# Connect to MongoDB
client = MongoClient(
                      host
                     ,port
                     ,username = username
                     ,password = password
                     ,authSource  = authSource 
                    )  # assuming your MongoDB container is named 'mongodb'

# Get the database
db = client['csv2json']
print("csv2json db 建立完成!")

# Get the collection
collection = db['csv2json']
print("csv2json collection 建立完成!")

# 讀取 JSON 檔案
with open('../試題一/csv2json.json', 'r', encoding='utf-8') as file:
    json_data = json.load(file)
    collection.insert_many(json_data)

