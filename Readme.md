# 程式執行說明

## 試題1
1. 進入試題一資料夾，並執行 mycode.py，所使用套件為 python 內建套件(csv、json、collections)
2. 執行完 mycode.py 後，會產生 csv2json.json 檔案，檔案內容為試題一要求之格式

## 試題2
1. 先使用 docker compose 建立 mongodb
2. 進入試題二資料夾，並執行 json_to_mongo.py，會建立於 mongodb 內建立 csv2json 的 collection，並讀取 csv2json.json 檔案將寫入，所使用套件為內建套件 (pymongo)，請參照 requirements.txt 安裝
3. docker_mongodb1 至 docker_mongodb4 為寫入 mongodb 後的截圖證明