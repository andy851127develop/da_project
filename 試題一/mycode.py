import csv
import json
from collections import defaultdict, OrderedDict

class CSV_TO_JSON:
    def __init__(self):
        # 設定 CSV、JSON 檔案的路徑
        self.source_file_path = 'CSV2JSON.csv'
        self.output_file_path = 'csv2json.json'

    # 讀取 CSV 文件並將其轉換為所需的 JSON 格式
    def csv_to_json(self):
        members = defaultdict(lambda: {'_id': '', 'member_id': '', 'tags': defaultdict(list)})

        # 打開 CSV 文件，使用 utf-8-sig 編碼來處理可能存在的 BOM
        with open(self.source_file_path, mode = 'r', newline = '', encoding = 'utf-8-sig') as file:
            # 使用 DictReader 以字典形式讀取 CSV 文件
            csv_reader = csv.DictReader(file) 
            for row in csv_reader:
                _id = row['member_id']
                member_id = row['member_id']
                tag_name = row['tag_name']
                detail_name = row['detail_name']
                detail_value = row['detail_value']

                # 將讀取到的資料填入 members 結構中
                members[member_id]['_id'] = _id
                members[member_id]['member_id'] = member_id
                members[member_id]['tags'][tag_name].append({
                    'detail_name': detail_name,
                    'detail_value': detail_value
                })

        # 構建最終的 JSON 結構
        result = []
        for member_id, member_info in members.items():
            # 將 tags 從 defaultdict 轉換為普通列表
            tags = [{'tag_name': tag_name, 'detail': details} for tag_name, details in member_info['tags'].items()]
            member_info['tags'] = tags

            # 使用 OrderedDict 做 Key 順序處理
            ordered_member_info = OrderedDict([
                ('_id', member_info['_id']),
                ('member_id', member_info['member_id']),
                ('tags', member_info['tags'])
            ])
            result.append(ordered_member_info)

        return result

# 將 JSON 數據寫入檔案
    def write_to_json(self,json_data):
        with open(self.output_file_path, 'w', encoding = 'utf-8') as json_file:
            json.dump(json_data, json_file, ensure_ascii = False, indent = 4)
            # ensure_ascii = False: 指定是否確保所有非 ASCII 字符在輸出中被轉義。設為 False 時，輸出中將保留非 ASCII 字符（如中文字符），這樣可以讓 JSON 文件更具可讀性。
            # indent = 4: 指定縮進級別，使用 4 個空格來進行縮排，使輸出的 JSON 格式更加美觀和易讀。

    def csv_to_json_file(self):
        # 將 CSV 轉換為 JSON
        json_data = self.csv_to_json()
        self.write_to_json(json_data)
        print("CSV 檔案已成功轉換為 JSON 格式")

if __name__ == '__main__':
    csv_to_json = CSV_TO_JSON()
    csv_to_json.csv_to_json_file()
